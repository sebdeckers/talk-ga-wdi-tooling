# tooling 🛠

Speaker notes for my talk at General Assembly Singapore to the Web Development Immersive class.

## Tools Make the Man

🙂 = 🐵

🙂 + 🚜 = 👨‍🌾

🙂 + 🔬 = 👨‍🔬

🙂 + 🚀 = 👩‍🚀

## High & Low Level

High level tools afford new use cases by reducing the cost of operations towards zero.

- [Chrome DevTools](https://developer.chrome.com/devtools)
- [Firefox Developer Tools](https://developer.mozilla.org/son/docs/Tools)
- [Safari Web Inspector](https://developer.apple.com/library/content/documentation/AppleApplications/Conceptual/Safari_Developer_Guide/Introduction/Introduction.html)
- [Edge F12](https://docs.microsoft.com/en-us/microsoft-edge/f12-devtools-guide)

Low level tools offer insight and deeper understanding by inflating the cost towards infinity.

- [Webpack Bundle Analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer)
- [Wireshark](https://www.wireshark.org)
- [MacOS Activity Monitor](https://support.apple.com/en-sg/HT201464) & [Windows Task Manager](https://support.microsoft.com/en-sg/help/323527/how-to-use-and-troubleshoot-issues-with-windows-task-manager)
- [Digital Logic Analyzer](https://www.saleae.com)

## Virtues of a Programmer

> "We will encourage you to develop the three great virtues of a programmer: laziness, impatience, and hubris." — Larry Wall, Programming Perl (from [C2 Wiki](http://wiki.c2.com/?LazinessImpatienceHubris))

Laziness makes you create custom scripts, plugins, or other types of programmes to automate chores. For example automated tests.

Impatience makes you write such well-performing code that it unlocks entirely new use cases. For example as-you-type validation or live search.

Hubris makes you write clean, elegant code that you can walk away from, forget about entirely, and yet have no problems returning to it if the need arises. For example linting & CI/CD.

## Custom Tools

### One-off Scripts & Configuration Files

Bespoke deployment steps or project settings.

See: Bash or YML files, Gulp, Grunt, NPM Scripts, Makefile, etc.

E.g. YML/JSON files in <https://gitlab.com/sebdeckers/lgtm/tree/master>

### Plugins

Reusable configurations or tasks that are useful across projects.

See: Babel, Webpack, ESLint, etc.

E.g. <https://gitlab.com/sebdeckers/eslint-plugin-webdriverio>
E.g. <https://www.npmjs.com/package/express-history-api-fallback>

### Frameworks

Customisable programmes that perform tasks which are not intrinsic part of the main project.

E.g. <https://gitlab.com/sebdeckers/unbundle>

## Code Reuse Gradient

1. Write some code
1. Reuse code within the same file by moving it to a function
1. Reuse code within a programme by moving it to a file
1. Reuse a programme by moving it to a package (NPM)
1. Write some code that uses the package, see 1.

## Further Reading

- <https://github.com/sindresorhus/awesome>
- <https://github.com/moimikey/awesome-devtools>
